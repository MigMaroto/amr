# Demo de Robot Mesero

## Conexión con Jetson Nano/Robot

Para controlar al robot es necesario disponer de una computadora externa. Ambos, la computadora externa y el robot deben estar conectados a la misma red. Luego seguir los siguientes pasos desde la computadora externa:

1. Escanear las IP asignadas por la red:

```shell
nmap -sP 192.168.1.0/24
```

2. Intentar la conexión ssh con todas las IP obtenidas:

```shell
ssh sasilva@{ip}
```

Una vez que la IP sea reconocida y aceptada, ingresar la contraseña: 12345678

## Configuración de ROS Master y esclavo

### Computadora externa

1. Reconocer IP de computadora:

```shell
ifconfig
```

2. Abrir .bashrc:

```shell
cd
nano .bashrc
```

3. Agregar o modificar las siguientes lineas:

```shell
export ROS_MASTER_URI=http://{IP_PC_EXTERNA}:11311
export ROS_IP={IP_PC_EXTERNA}
source /home/$USER/amr/amr_ws/devel/setup.bash
```

### Jetson Nano/Robot

Seguir los mismos pasos anteriores una vez que ya este conectado a al robot por ssh.

1. Abrir .bashrc:

```shell
cd
nano .bashrc
```

2. Agregar o modificar las siguientes lineas:

```shell
export ROS_MASTER_URI=http://{IP_ROBOT}:11311
export ROS_IP={IP_ROBOT}
```

### Prueba de conexión con ROS Master

Cerrar y abrir las terminales.

#### Computador externo

```shell
roscore
```

#### Robot

Conectado por ssh desde una terminal

```shell
rostopic list
```

En la terminal del robot, se deberia obtener el siguiente output:

```
/rosout
/rosout_agg
```

## Prueba de teleoperación

### Computador externo

#### Terminal # 1

```shell
roslaunch social_nav_tests robot_starter.launch
```

#### Terminal # 2

```shell
rosrun turtlebot3_teleop turtlebot3_teleop_key
```

#### Ver RVIz

Para poder ver rviz, abrir una terminal adicional y correr:

```shell
rviz
```

Luego `File` -> `Recent Configs` -> `{SEGUNDA CONFIGURACION}`.

### Robot

#### Terminal # 3

```shell
sudo chmod 777 /dev/ttyUSB*
roslaunch cartographer_launcher arlobot_exp_map_jetson.launch
```
